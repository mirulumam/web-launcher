package id.rumahbatik.weblauncher;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Konfigurasi extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.konfigurasi);
        final SharedPreferences preferences = getSharedPreferences("preferensi", MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        final String url = preferences.getString("url", "");
        final EditText editText = (EditText) findViewById(R.id.inputURL);
        editText.setHint(url);
        Button button = (Button) findViewById(R.id.buttonSave);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editor.putString("url", editText.getText().toString());
                    editor.commit();
                    Toast.makeText(Konfigurasi.this, "Berhasil disimpan: \n" + editText.getText(), Toast.LENGTH_SHORT).show();
                }
            });
        Button go = (Button) findViewById(R.id.buttonGo);
            go.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(url)
                    );
                    try {
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("buka web: ", "error");
                    }
                    Konfigurasi.this.finish();
                }
            });
    }
}
