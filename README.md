Aplikasi sederhana permintaan sahabat, ingin memubuat sebuah launcher menuju halaman web tertentu

Pemakaian:
- Pada activity Konfigurasi, dijumpai satu kotak input untuk memasukkan URL yang ingin dituju.
URL ini akan disimpan pada preferensi aplikasi
- Pada activity Launch, pengguna akan dibawa menuju URL yang sudah disimpan pada activity Konfigurasi.
Activity ini otomatis akan membuka URL tersimpan dengan browser default ponsel


// Rumah BATIK
// Muhammad Chairul Umam
// Bekasi, 22 Maret 2015
